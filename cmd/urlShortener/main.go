package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"urlShortener/internal/app/service"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	app := service.NewService(ctx)
	wg := sync.WaitGroup{}

	wg.Add(1)
	go app.Serve(ctx, &wg)

	<-ctx.Done()
	cancel()
	wg.Wait()

}
