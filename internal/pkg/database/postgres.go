package database

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"time"
	"urlShortener/internal/app/shortener"
)

type Client interface {
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
}

type Postgres struct {
	database Client
}

func NewConnect(ctx context.Context, connStr string) (*Postgres, error) {
	db, err := pgxpool.Connect(ctx, connStr)
	if err != nil {
		return nil, err
	}
	return &Postgres{database: db}, nil
}

func (p Postgres) CreateUrl(ctx context.Context, url *shortener.UserUrl) error {
	query := fmt.Sprintf("INSERT INTO url (url,shorturl, uuid) VALUES ('%s','%s','%s')", url.URL, url.ShortURL, url.Uuid)
	_, err := p.database.Query(ctx, query)
	return err
}

func (p Postgres) GetUrl(ctx context.Context, URL string) (*shortener.UserUrl, error) {
	query := fmt.Sprintf("select url,uuid from url where url.shorturl = '%s'", URL)
	data := p.database.QueryRow(ctx, query)
	url := shortener.UserUrl{}
	err := data.Scan(&url.URL, &url.Uuid)
	if err != nil {
		return nil, err
	}
	return &url, err
}

func (p Postgres) GetStatic(ctx context.Context, URL string) ([]shortener.Statistic, error) {
	url, err := p.GetUrl(ctx, URL)
	if err != nil {
		return nil, err
	}
	allStatistic := []shortener.Statistic{}
	queryStatistic := fmt.Sprintf("select ip, uuid, time from statistics where statistics.uuid = '%s'", url.Uuid)
	dataStatistic, err := p.database.Query(ctx, queryStatistic)
	for dataStatistic.Next() {
		s := shortener.Statistic{}
		err := dataStatistic.Scan(&s.IP, &s.Uuid, &s.Time)
		if err != nil {
			return nil, err
		}
		allStatistic = append(allStatistic, s)

	}

	return allStatistic, err
}

func (p Postgres) UpdateStatic(ctx context.Context, st *shortener.Statistic) error {
	query := fmt.Sprintf("INSERT INTO statistics (ip, time, uuid) VALUES ('%s','%s','%s')", st.IP, time.Now().Format(time.RFC3339), st.Uuid)
	_, err := p.database.Query(ctx, query)
	return err
}
