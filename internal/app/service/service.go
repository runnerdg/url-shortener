package service

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"log"
	"sync"
	"urlShortener/api/handler"
	"urlShortener/api/server"
	"urlShortener/internal/app/shortener"
	"urlShortener/internal/config"
	"urlShortener/internal/pkg/database"
)

type Implementation struct {
	Server *server.Server
}

func NewService(ctx context.Context) *Implementation {
	postgres, err := database.NewConnect(ctx, fmt.Sprintf("postgres://%s:%s@%s:%d/%s", config.User, config.Password, config.Host, config.Port, config.Database))
	if err != nil {
		log.Fatal(err)
	}
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err)
	}
	server := server.NewServer(
		handler.NewRouter(ctx, shortener.NewShortener(postgres, logger)),
		config.Add,
		*logger)
	return &Implementation{
		Server: server,
	}
}

func (i Implementation) Serve(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	i.Server.Serve()
	<-ctx.Done()
	i.Server.Stop(context.Background())

}
