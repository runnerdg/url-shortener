package shortener

import (
	"context"
	"fmt"
	"github.com/gofrs/uuid"
	"go.uber.org/zap"
	"log"
	"math/rand"
	"net/http"
	"time"
)

type ImplementationsDatabase interface {
	CreateUrl(ctx context.Context, url *UserUrl) error
	GetUrl(ctx context.Context, URL string) (*UserUrl, error)
	GetStatic(ctx context.Context, URL string) ([]Statistic, error)
	UpdateStatic(ctx context.Context, st *Statistic) error
}
type Shortener struct {
	database ImplementationsDatabase
	log      *zap.Logger
}

type UserUrl struct {
	URL      string
	ShortURL string
	Uuid     uuid.UUID
}

type Statistic struct {
	IP   string
	Time time.Time
	Uuid uuid.UUID
}

var (
	alphabet = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
)

func NewShortener(database ImplementationsDatabase, log *zap.Logger) *Shortener {
	return &Shortener{
		database: database,
		log:      log,
	}

}

func randomURL() string {
	b := make([]rune, len(alphabet))
	for i := range b {
		b[i] = alphabet[rand.Intn(len(alphabet))]
	}
	return string(b)[0:6]
}

func (s *Shortener) CreateUrl(ctx context.Context, URL string) (*UserUrl, error) {
	id, err := uuid.NewV4()
	if err != nil {
		log.Printf("Create uuid error: %s\n", err)
		return nil, fmt.Errorf("intenal service error: %d", http.StatusInternalServerError)
	}
	newURL := UserUrl{
		URL:      URL,
		ShortURL: randomURL(),
		Uuid:     id,
	}
	err = s.database.CreateUrl(ctx, &newURL)
	if err != nil {
		log.Printf("CreateUrl: %s\n", err)
		return nil, fmt.Errorf("intenal service error: %d", http.StatusInternalServerError)
	}

	return &newURL, err
}

func (s *Shortener) GetStatistic(ctx context.Context, URL string) ([]Statistic, error) {
	static, err := s.database.GetStatic(ctx, URL)
	if err != nil {
		log.Printf("GetStatistic :%s", err)
		return nil, fmt.Errorf("intenal service error: %d", http.StatusInternalServerError)
	}
	return static, nil
}

func (s *Shortener) Redirect(ctx context.Context, URL string, st *Statistic) (string, error) {
	shortUrl, err := s.database.GetUrl(ctx, URL)
	if err != nil {
		log.Printf("GetUrl :%s", err)
		return "", fmt.Errorf("intenal service error: %d", http.StatusInternalServerError)
	}
	st.Uuid = shortUrl.Uuid
	err = s.database.UpdateStatic(ctx, st)
	if err != nil {
		log.Printf("UpdateStatic :%s", err)
		return "", fmt.Errorf("intenal service error: %d", http.StatusInternalServerError)
	}

	return shortUrl.URL, err
}
