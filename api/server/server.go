package server

import (
	"context"
	"crypto/tls"
	"go.uber.org/zap"
	"golang.org/x/crypto/acme/autocert"
	"net/http"
	"time"
)

type Server struct {
	srv    http.Server
	srvTLS http.Server
	log    *zap.Logger
}

func NewServer(mux http.Handler, Addr string, log zap.Logger) *Server {
	certManager := autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		Cache:      autocert.DirCache("cert-cache"),
		HostPolicy: autocert.HostWhitelist("localhost"),
	}
	s := &Server{log: &log}
	s.srv = http.Server{
		Addr:    Addr,
		Handler: mux,
	}
	s.srvTLS = http.Server{
		Addr:    ":443",
		Handler: mux,
		TLSConfig: &tls.Config{
			GetCertificate: certManager.GetCertificate,
		},
	}
	return s
}

func (s *Server) Serve() {

	go func() {
		err := s.srv.ListenAndServe()
		if err != nil {
			s.log.Fatal(err.Error())
		}
	}()

	go func() {
		err := s.srvTLS.ListenAndServeTLS("", "")
		if err != nil {
			s.log.Fatal(err.Error())
		}
	}()

}

func (s *Server) Stop(ctx context.Context) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)

	err := s.srv.Shutdown(ctx)
	if err != nil {
		s.log.Error(err.Error())
	}
	cancel()

}
