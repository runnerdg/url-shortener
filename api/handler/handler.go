package handler

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
	"net/http"
	"strings"
	"urlShortener/internal/app/shortener"
)

type ImplementationsShortener interface {
	CreateUrl(ctx context.Context, URL string) (*shortener.UserUrl, error)
	GetStatistic(ctx context.Context, URL string) ([]shortener.Statistic, error)
	Redirect(ctx context.Context, URL string, st *shortener.Statistic) (string, error)
}

type Router struct {
	http.Handler
	shortener ImplementationsShortener
	ctx       context.Context
}

func NewRouter(ctx context.Context, shortener ImplementationsShortener) *Router {
	r := &Router{
		shortener: shortener,
		ctx:       ctx,
	}
	gh := gin.Default()
	gh.Use(cors.Default())
	gh.POST("/create", r.Create)
	gh.GET("/static/:path", r.Static)
	gh.GET("/:path", r.Redirect)
	r.Handler = gh
	return r

}

func (r *Router) Redirect(c *gin.Context) {
	st := shortener.Statistic{
		IP: c.Request.RemoteAddr,
	}
	redirect, err := r.shortener.Redirect(r.ctx, c.Request.URL.Path[1:], &st)
	if err != nil {
		c.String(500, "%s", err.Error())
	}
	c.Redirect(308, redirect)
}
func (r *Router) Static(c *gin.Context) {
	static, err := r.shortener.GetStatistic(r.ctx, strings.Replace(c.Request.URL.Path, "static/", "", -1)[1:])
	if err != nil {
		c.String(500, "%s", err.Error())
	}

	c.JSON(http.StatusOK, static)
}

func (r *Router) Create(c *gin.Context) {
	var data ShortURL
	err := c.BindJSON(&data)
	if err != nil {
		c.String(500, "Invalid body %s", err.Error())
	}
	url, err := r.shortener.CreateUrl(r.ctx, data.Url)
	if err != nil {
		c.String(500, "%s", err.Error())
	}
	fmt.Println(url.ShortURL)
	c.JSON(http.StatusOK, url.ShortURL)
}

type ShortURL struct {
	Url string `json:"url"`
}
