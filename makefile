.PHONY: run
run:
	go run cmd/urlShortener/main.go

.PHONY: build
build:
	go mod tidy
	go build -o bin cmd/urlShortener/main.go 


.PHONY: lint
lint: 
	golangci-lint --timeout 5m0s run ./...

.PHONY: install-lint
install-lint: 
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.41.1


.PHONY: test
test:
	go test -race  -cover -v ./...
