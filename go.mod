module urlShortener

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/jackc/pgx/v4 v4.13.0
	github.com/rs/cors v1.8.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.19.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
