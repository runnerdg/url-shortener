FROM golang:1.17
WORKDIR /go/src/app
COPY . .

#RUN apk add --no-cache ca-certificates git

VOLUME ["/cert-cache"]

CMD ["./bin"]
EXPOSE 443
EXPOSE 80